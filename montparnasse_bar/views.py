from django.shortcuts import render, get_list_or_404
from .models import Beer, Bar, Stock
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.admin.views.decorators import staff_member_required
from .form import ReferenceForm, BarForm


@login_required
def reference(request):
    references = get_list_or_404(Beer)
    page = request.GET.get('page', 1)
    paginator = Paginator(references, 10)
    try:
        references = paginator.page(page)
    except PageNotAnInteger:
        references = paginator.page(1)
    except EmptyPage:
        references = paginator.page(paginator.num_pages)
    form = ReferenceForm()
    return render(request, 'montparnasse_bar/references.html', {'references': references, 'form': form})


@staff_member_required
def reference_modify(request):
    if 'delete' in request.POST:
        beer = get_list_or_404(Beer, id=request.POST['choice'])
        for b in beer:
            b.delete()
    elif 'add' in request.POST:
        beer = Beer()
        beer.ref = request.POST['ref']
        beer.name = request.POST['name']
        beer.description = request.POST['description']
        beer.save()
    elif 'modify' in request.POST:
        beers = get_list_or_404(Beer, id=request.POST['id'])
        for beer in beers:
            beer.ref = request.POST['reference']
            beer.name = request.POST['name']
            beer.description = request.POST['description']
            beer.save()
    return HttpResponseRedirect(reverse('montparnasse_bar:reference'))


@login_required
def bar(request):
    bars = get_list_or_404(Bar)
    page = request.GET.get('page', 1)
    paginator = Paginator(bars, 10)
    try:
        bars = paginator.page(page)
    except PageNotAnInteger:
        bars = paginator.page(1)
    except EmptyPage:
        bars = paginator.page(paginator.num_pages)
    form = BarForm()
    return render(request, 'montparnasse_bar/bars.html', {'bars': bars, 'form': form})


@staff_member_required
def bar_modify(request):
    if 'delete' in request.POST:
        bar = get_list_or_404(Bar, id=request.POST['choice'])
        for b in bar:
            b.delete()
    elif 'add' in request.POST:
        bar = Bar()
        bar.id = request.POST['pk']
        bar.name = request.POST['name']
        bar.save()
    elif 'modify' in request.POST:
        bars = get_list_or_404(Bar, id=request.POST['id'])
        for bar in bars:
            bar.name = request.POST['name']
            bar.save()
    return HttpResponseRedirect(reverse('montparnasse_bar:bar'))


@login_required
def stock(request, bar_id):
    stocks = Stock.objects.all()
    # stocks = get_list_or_404(Stock, bar_id=bar_id)
    page = request.GET.get('page', 1)
    paginator = Paginator(stocks, 10)
    try:
        stocks = paginator.page(page)
    except PageNotAnInteger:
        stocks = paginator.page(1)
    except EmptyPage:
        stocks = paginator.page(paginator.num_pages)
    return render(request, 'montparnasse_bar/stock.html', {'stocks': stocks})


@login_required
def ranking(request):
    bars_miss = get_list_or_404(Stock, stock=0)
    bars = get_list_or_404(Bar)
    all = {bar.id for bar in bars}
    miss = {bar.bar_id for bar in bars_miss}
    all -= miss
    return render(request, 'montparnasse_bar/ranking.html', {'all': list(all), 'miss': list(miss)})


def menu(request):
    stocks = get_list_or_404(Stock)
    available = dict()
    unavailable = dict()
    for stock in stocks:
        beer = stock.beer
        if stock.stock > 0:
            if beer.id in unavailable:
                unavailable.pop(beer.id)
            available[beer.id] = beer
        elif beer.id not in available:
            unavailable[beer.id] = beer
    results = list()
    for id_, beer in available.items():
        results.append({'ref': beer.ref, 'name': beer.name, 'description': beer.description,
                        'availibility': 'available'})
    for id_, beer in unavailable.items():
        results.append({'ref': beer.ref, 'name': beer.name, 'description': beer.description,
                        'availibility': 'out of stock'})
    page = request.GET.get('page', 1)
    paginator = Paginator(results, 10)
    try:
        results = paginator.page(page)
    except PageNotAnInteger:
        results = paginator.page(1)
    except EmptyPage:
        results = paginator.page(paginator.num_pages)
    return render(request, 'montparnasse_bar/menu.html', {'results': results})

