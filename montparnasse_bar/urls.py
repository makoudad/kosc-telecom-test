from django.urls import path
from django.views.generic.base import TemplateView

from . import views

app_name = 'montparnasse_bar'
urlpatterns = [
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
    path('api/reference', views.reference, name='reference'),
    path('api/reference/modify', views.reference_modify, name='reference_modify'),
    path('api/bars', views.bar, name='bar'),
    path('api/bars/modify', views.bar_modify, name='bar_modify'),
    path('api/stock/<int:bar_id>', views.stock, name='stock'),
    path('api/bars/ranking', views.ranking, name='ranking'),
    path('api/menu', views.menu, name='menu'),
]