from django import forms


class ReferenceForm(forms.Form):
    ref = forms.CharField(label='Ref', max_length=200)
    name = forms.CharField(label='Name', max_length=200)
    description = forms.CharField(label='Description', max_length=200)


class BarForm(forms.Form):
    pk = forms.IntegerField(label='Name')
    name = forms.CharField(label='Etage', max_length=200)
