from django.contrib import admin
from .models import Beer, Bar, Stock

admin.site.register(Beer)
admin.site.register(Bar)
admin.site.register(Stock)