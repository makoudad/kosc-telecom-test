from django.db import models


class Beer(models.Model):
    ref = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)


class Bar(models.Model):
    name = models.CharField(max_length=200)


class Stock(models.Model):
    bar = models.ForeignKey(Bar, on_delete=models.CASCADE)
    beer = models.ForeignKey(Beer, on_delete=models.CASCADE)
    stock = models.IntegerField(default=0)



