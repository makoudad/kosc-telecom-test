from django.apps import AppConfig


class MontparnasseBarConfig(AppConfig):
    name = 'montparnasse_bar'
